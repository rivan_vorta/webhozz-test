import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

// Screens
import MainNavigator from './MainNavigator';
import DetailScreen from '../screens/Detail';
import ListScreen from '../screens/List';

const Stack = createStackNavigator();

const AppStack = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }} >
      <Stack.Screen name="Main" component={MainNavigator} />
      <Stack.Screen name="Detail" component={DetailScreen} />
      <Stack.Screen name="List" component={ListScreen} />
    </Stack.Navigator>
  );
};

export default AppStack;
