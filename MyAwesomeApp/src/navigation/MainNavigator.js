import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons'

import Home from '../screens/Home';
import Profile from '../screens/Profile';

const Tab = createBottomTabNavigator();

const MainNavigator = () => {
    return (
        <Tab.Navigator screenOptions={({ route }) => ({
            headerShown: false,
            tabBarActiveTintColor: 'blue',
            tabBarInactiveTintColor: 'grey',
            tabBarStyle: {
                height: 65
            },
            tabBarItemStyle: {
                paddingVertical: 5,
            },
            tabBarLabelStyle: {
                fontSize: 12
            },
            tabBarIcon: ({ focused, size, color }) => {
                let iconName;

                if(route.name === 'Home') {
                    iconName = focused ? 'home' : 'home-outline'
                } else if(route.name === 'Profile') {
                    iconName = focused ? 'person' : 'person-outline'
                }

                return <Icon name={iconName} size={size} color={color} />
            }
        })}>
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Profile" component={Profile} />
        </Tab.Navigator>
    );
};

export default MainNavigator;
