import React from 'react';
import { View, Text, Button, StyleSheet, FlatList } from 'react-native';
import data from '../data/data.json'

const ListScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.txt}>List Screen</Text>
            <FlatList
                data={data}
                keyExtractor={(item, index) => index}
                style={{ width: '100%' }}
                renderItem={({ item }) => (
                    <View style={styles.card}>
                        <Text style={styles.cardTxt}>Name : {item.name}</Text>
                        <Text style={styles.cardTxt}>Age : {item.age}</Text>
                    </View>
                )}
            />
            <Button title='Go back to Home Screen' onPress={() => navigation.goBack()} />
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        backgroundColor: 'lightblue'
    },
    txt: {
        fontSize: 40,
        fontWeight: 'bold',
        color: 'black'
    },
    card: {
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 8,
        padding: 20,
        marginTop: 10,
        width: '80%',
        alignSelf:'center'
    },
    cardTxt: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black'
    }
});

export default ListScreen;