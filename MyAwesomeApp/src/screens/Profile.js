import React from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';

const ProfileScreen = () => {
    return (
        <ScrollView>
            <View style={styles.container}>
                <Text style={styles.txt}>Profile Screen</Text>
                <Text style={styles.smallTxt}>Secara Default Komponen pada React Native sudah berdisplay Flexbox namun mengarah kebawah atau flex direction: column</Text>
                <View style={styles.flexContainer}>
                    <View style={styles.kotak1} />
                    <View style={styles.kotak2} />
                    <View style={styles.kotak3} />
                </View>

                <Text style={styles.smallTxt}>Kita bisa mengatur nya sesuai keinginan yang dibutuhkan</Text>
                <View style={styles.flexContainer1}>
                    <View style={styles.kotak1} />
                    <View style={styles.kotak2} />
                    <View style={styles.kotak3} />
                </View>

                <Text style={styles.smallTxt}>Dan bisa mengatur jarak nya secara relative dan otomatis</Text>
                <View style={styles.flexContainer2}>
                    <View style={styles.kotak1} />
                    <View style={styles.kotak2} />
                    <View style={styles.kotak3} />
                </View>
            </View>
        </ScrollView>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: 'lightblue'
    },
    txt: {
        fontSize: 40,
        fontWeight: 'bold',
        color: 'black'
    },
    smallTxt: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        marginVertical: 15
    },
    kotak1: {
        width: 100,
        height: 100,
        backgroundColor: 'red'
    },
    kotak2: {
        width: 100,
        height: 100,
        backgroundColor: 'blue'
    },
    kotak3: {
        width: 100,
        height: 100,
        backgroundColor: 'green'
    },
    flexContainer: {
        borderColor: 'black',
        borderWidth: 1,
        padding: 10,
        width: '100%'
    },
    flexContainer1: {
        borderColor: 'black',
        borderWidth: 1,
        padding: 10,
        width: '100%',
        flexDirection: 'row'
    },
    flexContainer2: {
        borderColor: 'black',
        borderWidth: 1,
        padding: 10,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
})

export default ProfileScreen;