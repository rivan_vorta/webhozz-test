import React from 'react';
import { View, Text, Button, StyleSheet,TextInput } from 'react-native';

const HomeScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.txt}>Home Screen</Text>
            <TextInput
                placeholder='Input Here..'
                style={styles.txtInput}
            />
            <View>
                <Button title='Go to Detail Screen' onPress={() => navigation.navigate('Detail')} />
                <View style={{ marginVertical: 10 }} />
                <Button title='Go to List Screen' onPress={() => navigation.navigate('List')} />
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        backgroundColor: 'lightblue'
    },
    txt: {
        fontSize: 40,
        fontWeight: 'bold',
        color: 'black'
    },
    txtInput: {
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 8,
        paddingVertical: 10,
        paddingHorizontal: 25,
        fontSize: 20,
        color: 'black',
        width: '100%',
        backgroundColor: 'white'
    }
});

export default HomeScreen;