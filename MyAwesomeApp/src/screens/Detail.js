import React, { useState } from 'react';
import { View, Text, Button, StyleSheet, Modal, TextInput, Pressable } from 'react-native';

const DetailScreen = ({ navigation }) => {
    const [isVisible, setIsVisible] = useState(false);
    const [name, setName] = useState();
    const [email, setEmail] = useState();

    return (
        <View style={styles.container}>
            <Text style={styles.txt}>Detail Screen</Text>
            <View>
                <TextInput
                    placeholder='Input Name..'
                    style={styles.txtInput}
                    onChangeText={text => setName(text)}
                    />
                <TextInput
                    placeholder='Input Email..'
                    style={styles.txtInput}
                    onChangeText={text => setEmail(text)}
                />
                <Button
                    title='Klik disini untuk menampilkan Modal'
                    onPress={() => setIsVisible(true)}
                />
            </View>
            <Button
                title='Go back to Home Screen'
                onPress={() => navigation.goBack()}
            />
            <Modal
                visible={isVisible}
                animationType='slide'
                hardwareAccelerated
                transparent
                onRequestClose={() => setIsVisible(false)}
            >
                <Pressable style={styles.modalContainer} onPress={() => setIsVisible(false)}>
                    <View style={styles.modalBox}>
                        <Text style={styles.modalText}>Nama :</Text>
                        <Text style={styles.modalText}>{name ? name : 'Kamu belum memasukkan Nama mu'}</Text>
                        <Text style={styles.modalText}>Email :</Text>
                        <Text style={styles.modalText}>{email ? email : 'Kamu belum memasukkan Email mu'}</Text>
                    </View>
                </Pressable>
            </Modal>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        justifyContent: 'space-between',
        backgroundColor: 'lightblue'
    },
    txt: {
        fontSize: 40,
        fontWeight: 'bold',
        color: 'black'
    },
    txtInput: {
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 8,
        paddingVertical: 10,
        paddingHorizontal: 25,
        marginBottom: 20,
        fontSize: 20,
        color: 'black',
        width: '100%',
        backgroundColor: 'white'
    },
    modalContainer: {
        flex: 1,
        backgroundColor: 'rgba(55, 55, 55, 0.3)',
        justifyContent: 'center',
        padding: 20
    },
    modalBox: {
        backgroundColor: 'white',
        borderWidth: 1,
        borderRadius: 8,
        padding: 15
    },
    modalText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        marginVertical: 5
    }
})

export default DetailScreen;