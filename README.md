# React Native

Sebelum menjalankan aplikaksi ini, pastikan PC yang digunakan sudah terdapat Environment yang dibutuhkan untuk menjalakan aplikasi React Native.

Cara Menjalankan Aplikasi ini :

1. Masuk kedalam folder project dan jalankan command dibawah ini melalui terminal :
```bash
yarn
```

2. Jalankan metro server dengan :
```bash
yarn start
```

3. Buka Terminal baru dan install aplikasi pada Emulator / Physical Android Device :
```bash
yarn android
```

Jawaban Soal bisa ditemukan pada file `Jawab.md`

[Jawab.md](Jawab.md)