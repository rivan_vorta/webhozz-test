# React Native

## 1. Props & State

State adalah seperti sebuah Variable yang mana digunakan untuk menyimpan sebuah nilai.

Props (Properties) adalah sebuah seperti sebuah Argumen yang diberikan kepada sebuah Fungsi, yang mana Argumen tersebut bisa digunakan didalam Fungsi / Komponen tertentu.

```javascript
const ComponentA = (props) => {
    // Properties yang diterima kemudian di destructure
    const { nama } = props;

    return (
        <View>
            {/* Properties tersebut digunakan untuk ditampilkan pada komponen */}
            <Text>{nama}</Text>
        </View>
    )
}

const ComponentB = () => {
    // State pada functional component
    // yang mana seperti sebuah karakter sebuah komponen / variable yang menyimpan nilai
    const [namaState, setNamaState] = useState('nilai awal');

    return (
        // Menampilkan Component A
        <ComponentA
            // Memberikan props pada Component A
            nama='Steve Jobs'
        />
    )
}
```

## 2. Aplikasi MultiPage dengan Navigasi

Project ini telah mengimplementasikan aplikasi multipage dengan navigasi.

## 3. Contoh Styling Form Input

Berikut adalah contoh styling form input :

```javascript
import { TextInput } from 'react-native';

const AppScreen = () => {
    return (
        <View>
            <TextInput
                placeholder='Input Here..'
                style={styles.txtInput}
            />
        </View>
    )
};

const styles = StyleSheet.create({
    txtInput: {
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 8,
        paddingVertical: 10,
        paddingHorizontal: 25,
        fontSize: 20,
        color: 'black',
        width: '100%',
        backgroundColor: 'white'
    }
});
```

Atau bisa juga dilihat pada Home Screen 

## 4. Contoh Layout dengan FlexBox

Berikut adalah contoh Layout dengan FlexBox :

```javascript
const AppScreen = () => {
    return (
        <View style={styles.flexContainer}>
            <View style={styles.kotak1} />
            <View style={styles.kotak2} />
            <View style={styles.kotak3} />
        </View>
    )
};

const styles = StyleSheet.create({
    flexContainer: {
        borderColor: 'black',
        borderWidth: 1,
        padding: 10,
        width: '100%'
    },
    kotak1: {
        width: 100,
        height: 100,
        backgroundColor: 'red'
    },
    kotak2: {
        width: 100,
        height: 100,
        backgroundColor: 'blue'
    },
    kotak3: {
        width: 100,
        height: 100,
        backgroundColor: 'green'
    }
});
```

Atau bisa juga dilihat implementasinya pada Profile Screen

## 5. Input Nama dan Email pada Modal

Contoh implementasi bisa dilihat pada Detail Screen

## 6. Menampilkan data JSON dengan listview

Contoh implementasi bisa dilihat pada List Screen

## 7. POST data menggunakan API ke Database

Berikut adalah contoh untuk membuat data baru (POST) dengan API ke Database pada React Native :

```javascript
// Menggunakan Fetch (Native JavaScript)
const getDataWithFetch = () => {
    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify({
            title: 'Title Text',
            body: 'This is Description body',
            userId: 1
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(json => json.json())
    .then(res => console.log(res))
    .catch(err => console.log(err));
};

// Menggunakan Salah satu library HTTP Request (Axios)
const getDataWithAxios = () => {
    axios({(
        method: 'POST',
        url: 'https://jsonplaceholder.typicode.com/posts',
        data: {
            title: 'Title Text',
            body: 'This is Description body',
            userId: 1
        },
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => console.log(res))
    .catch(err => console.log(err));
};
```

## 8. Input data ke SQLite

Berikut adalah contoh input data ke SQLite pada React Native.

```javascript
import { openDatabase } from 'react-native-sqlite-storage';

const db = openDatabase(
    { name: 'my-db', createFromLocation: 1 },
    () => {
        alert('Berhasil Menyambungkan ke Database');
    },
    (err) => {
        console.log('Error Menyambungkan ke Database = ', err);
    }
)

const handleInputData = (value) => {
    db.transaction((tx) => {
        tx.executeSql(
            'INSERT INTO user (user_name) VALUES (?);)',
            [value],
            (txn, res) => {
                if(res.rowsAffected > 0) {
                    let newData = [];
                    newData.push(value);
                }
            },
            (err) => {
                console.log(err);
            }
        )
    })
};
```